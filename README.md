# Running the app
```
    yarn start
    OR
    npm run start
```
node version - v6.14.6


Make sure the backend is running locally on port 8080


### Dev Process


#### Day 1 (Saturday): 10AM

Spent time learning react patterns and creating basic boilerplate project that I can use to get myself started and get comfortable quickly.. Got started with basic component structure, router, some basic redux set up(just in case)

3PM -Break
7PM - Was going through some reactjs docs to better understand the good practices out there, a lot of patterns changed since I last used React

#### Day 2 9AM- 

Once I started getting more comfortable, I went ahead and started building the pages,
separated the links into 2 different pages. Since the router structure was already setup, It was easy to get started

10AM - Started building some basic placeholder components, started the dev server locally and started understanding what needs to be done for the booking app

1PM - Got some event handler functions and interactive elements to work and make it more polished, now is when the UI started shaping up like the design docs, started working on the logic, apparently the logic is too big to handled in a single function, this is also the time Ive been contemplating the use of Redux.
I kept breaking down the unit functions into components, I noticed I've come to a point where I've been passing props 3 levels down for children to use. But before I could refactor the state into redux, I wanted a basic working app since its already almost evening

5PM - At this point Ive realized that the booking app isnt as easy as it looked, there is a lot of logic that goes hand in hand and dependent on each other. Spent more time looking into the logic, especially the grouping of shifts into "Days of the week", I remember doing this easily in the past using moment. Did some experimenting, read through the moment docs and used the group function to get it to work the way I want.

7PM: Polishing UI, at this point, I've managed to get the app working well and handled all the validations, wanted to create a loader like Facebook post loading animation, I build something very basic when the requests are loading

It was time to submit, I can conclude that it was a pretty challenging task, not as easy as it looked. I had fun working on it and I enjoyed it.

#### Improvements - Things I would change if I had more time

- I'd have worked on the state better, used redux to make the exchange of state more seamless between the components

- I had used just 2 scss files to store all the css, I'd break it down to files, used scss variables and used them over, was caught up in making the UI look as close to the design as possible and ran out of time and the colors are hardcoded all over. 

- Break down stuff into more sensible components that is easy to understand at first glance

- Wasnt able to name my components well, they were named hastily

- Refactor JS code into utils and reused them where necessary




