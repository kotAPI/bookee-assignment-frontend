// @flow

import React from "react";

import Normalize from 'react-normalize';

import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import NavBar from "./components/Navbar/Navbar"

import "./style/_index.scss"

import routeConfig from "./routes"

function App() {
    return (
        <Router>
            <Normalize/>
             <NavBar/>
            <Switch>
                <Repeater/>
            </Switch>
        </Router>
    );
  }

function Repeater(){

    return(
        <div>
           {routeConfig.routes.map((item)=>{
               return (
                   <Route path={item.path} component={item.component} exact={item.exact} key={item.path} />
               )
           })}
        </div>
    )
  }


export default App;