

import Home from "./pages/Home/Home.js"
import AvailableShifts from "./pages/AvailableShifts/AvailableShifts.js"


let router =  {
    routes: [
        {path: '/', component: Home, exact:true},
        {path: '/available-shifts', component: AvailableShifts},
    ]
}


export default router;