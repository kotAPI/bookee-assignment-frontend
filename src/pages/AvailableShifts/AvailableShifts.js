// @flow

import React from "react"

import _ from "lodash";
import TableRenderer from "../../components/TableRenderer/TableRenderer"
import TableLoading from "../../components/TableLoading/TableLoading"

const axios = require('axios');

class Home extends React.Component {


    constructor(props) {
        super(props)

        this.state = {
            shifts: [],
            requestInProcess: false
        }

        this.shiftChangeHandler = this.shiftChangeHandler.bind(this)
        this.updateShiftStatus = this.updateShiftStatus.bind(this);


    }


    async componentDidMount() {
        try {

            this.setState({ requestInProcess: true })
            let res = await axios.get("http://localhost:8080/shifts")
            this.setState({ shifts: res.data })
            this.setState({ requestInProcess: false })
        } catch (err) {
            this.setState({ requestInProcess: false })
        }
    }

    shiftChangeHandler(shift) {
        this.updateShiftStatus(shift)
    }

    updateShiftStatus(shift) {
        var itemIndex = _.findIndex(this.state.shifts, shiftItem => {
            return shiftItem.id === shift.id
        })
        var shiftsCopy = this.state.shifts;
        shiftsCopy[itemIndex] = shift;

        this.setState({ shifts: shiftsCopy })
    }

    render() {
        return (
            <div className="app-page">
                {this.state.requestInProcess ? <TableLoading /> : <TableRenderer shifts={this.state.shifts} key={this.state.shifts[0]}
                    context="AVAILABLE_SHIFTS"
                    shiftChange={this.shiftChangeHandler} />}

            </div>
        )

    }
}

export default Home;