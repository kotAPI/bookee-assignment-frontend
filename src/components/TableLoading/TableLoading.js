import _ from "lodash"
import React from "react"

class TableLoading extends React.Component {

    render() {
        return (
            <div className="table-renderer">
                <div className="section">
                    <div className="title">
                        <div className="title-loading"></div>
                    </div>
                    <div className="items">
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                    </div>
                    <div className="title">
                        <div className="title-loading"></div>
                    </div>
                    <div className="items">
                      
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                    </div>
                    <div className="title">
                        <div className="title-loading"></div>
                    </div>
                    <div className="items">
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>

                        <div className="item">
                            <div className="title-loading-container">
                                <div className="title-loading"></div>
                            </div>
                            <div className="button-loading-container">
                                <div className="button-loading"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>


            </div>
        )
    }

}

export default TableLoading