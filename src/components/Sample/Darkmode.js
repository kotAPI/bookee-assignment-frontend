import React from "react";

class DarkMode extends React.Component {

    render(){
        return(
            <div>
               {this.props.darkMode?<span>Dark mode is on</span>:<span>Dark mode is not on</span>}
            </div>
        )
    }
}

export default DarkMode