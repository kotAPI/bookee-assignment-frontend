import React from "react";
import "./_tableRenderer.scss"


import _ from "lodash";
import moment from "moment"

import ShiftGroupRenderer from "./Children/ShiftGroupRenderer"

class TableRenderer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: undefined,
            cityTabs: []
        }

        this.getUniqueCitiesWithCounter = this.getUniqueCitiesWithCounter.bind(this)
        this.selectTab = this.selectTab.bind(this)

        this.RenderTabs = this.RenderTabs.bind(this)

    }

    componentDidMount() {
        let uniqueCities = this.getUniqueCitiesWithCounter(this.props.shifts);

        this.setState({
            cityTabs: uniqueCities,
            activeTab: uniqueCities.length ? uniqueCities[0].name : undefined
        })
    }

    getUniqueCitiesWithCounter(shifts) {
        let cityMap = {};
        shifts.map(shift => {
            if (cityMap[shift.area]) {
                cityMap[shift.area].count++;
            } else {
                cityMap[shift.area] = {
                    count: 1,
                    name: shift.area
                }
            }

        })
        var cityArr = [];
        for (let city in cityMap) {
            cityArr.push(cityMap[city])
        }
        return cityArr
    }

    selectTab(tabItem) {
        this.setState({ activeTab: tabItem.name })
    }

    RenderTabs(cities) {

        return (
            <div className="tabs">
                {cities.map(element => {
                    return <div
                        className={(this.state.activeTab == element.name) ? 'tab-item active' : 'tab-item'}
                        onClick={() => {
                            this.selectTab(element)
                        }
                        }
                        key={element.name}>{element.name} ({element.count})</div>
                })}

            </div>
        )
    }


    render() {
        return (
            <div className="table-renderer">
                {this.props.context==='AVAILABLE_SHIFTS'?this.RenderTabs(this.state.cityTabs):''}
                <ShiftGroupRenderer 
                key={this.state.activeTab} 
                shifts={this.props.shifts} 
                activeTab={this.state.activeTab}
                shiftChange={this.props.shiftChange}
                context={this.props.context} />
            </div>
        )
    }
}





export default TableRenderer