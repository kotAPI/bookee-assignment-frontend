import React from "react";

import GreenSpinner from "./GreenSpinner"
import RedSpinner from "./RedSpinner"
import _ from "lodash";
import axios from "axios"


class ShiftButton extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            booked: false,
            requestInProgress: false,
            shiftStatus: {}
        }


        this.BookShift = this.BookShift.bind(this)
        this.CancelShift = this.CancelShift.bind(this)
        this.GreenButton = this.GreenButton.bind(this)
        this.RedButton = this.RedButton.bind(this)
        this.getShiftStatus = this.getShiftStatus.bind(this)
    }

    componentDidMount() {
        this.setState({ shiftStatus: this.getShiftStatus() })
    }

    getShiftStatus() {

        let now = new Date();
            let shiftStart = new Date(this.props.shift.startTime)
            let shiftEnd = new Date(this.props.shift.endTime)
            if (now > shiftStart) {
                return {
                    status: false,
                    reason: "SHIFT_ALREADY_STARTED"
                };
            }
            if (now > shiftEnd) {
                return {
                    status: false,
                    reason: "SHIFT_ALREADY_ENDED"
                };
            }

        // Copied the logic from the backend code :D
        if (this.props.context === "AVAILABLE_SHIFTS") {
            

            var isOverlapping = !!this.props.shifts.filter(shiftItem => shiftItem.booked)
                .find(shiftItem => shiftItem.startTime < this.props.shift.endTime && shiftItem.endTime > this.props.shift.startTime);;

            if (isOverlapping) {
                return {
                    status: false,
                    reason: "OVERLAPPING_SHIFT"
                }
            }
        }

        return {
            status: true,
            reason: "OK"
        };

    }

    async BookShift() {
        try {
            this.setState({ requestInProgress: true })
            let res = await axios.post(`http://localhost:8080/shifts/${this.props.shift.id}/book`)
            this.setState({ requestInProgress: false })
            this.props.shiftChange(res.data)
        } catch (err) {
            this.setState({ requestInProgress: false })
        }


    }

    async CancelShift() {
        try {
            this.setState({ requestInProgress: true })
            let res = await axios.post(`http://localhost:8080/shifts/${this.props.shift.id}/cancel`)
            this.setState({ requestInProgress: false })
            this.props.shiftChange(res.data)
        } catch (err) {
            this.setState({ requestInProgress: false })

        }


    }

    GreenButton() {
        return (
            <button onClick={this.BookShift}
                className={this.state.shiftStatus.status ? 'success' : 'success disabled'}>
                {this.state.requestInProgress ? <GreenSpinner /> : this.props.shift.booked ? 'Cancel' : 'Book'}
            </button>
        )
    }

    RedButton() {
        return (
            <button onClick={this.CancelShift}
                className={!this.state.shiftStatus.status ? 'danger disabled' : 'danger'}>
                {this.state.requestInProgress ? <RedSpinner /> : this.props.shift.booked ? 'Cancel' : ''}
            </button>
        )
    }


    render() {
        return (
            <div className="item">
                <div className="item-info">
                    {ShiftRenderer(this.props.context, this.props.shift)}
                </div>
                <div className="item-actions">
                    {this.props.shift.booked ? this.RedButton(this.props.shift) : this.GreenButton(this.props.shift)}

                </div>
            </div>

        )
    }
}


function ShiftRenderer(context, shift) {
    if (context === "MY_SHIFTS") {
        return (
            <div className="shift-info-container" >
                <div>{getHumanFriendlyShiftString(shift)}</div>
                <div className="shift-area">{shift.area}</div>
            </div>
        )
    } else {
        return (
            <div className="shift-info-container">
                <div>{getHumanFriendlyShiftString(shift)}</div>
            </div>
        )
    }
}

function getHumanFriendlyShiftString(shift) {
    let startTime = new Date(shift.startTime)
    let endTime = new Date(shift.endTime)

    return startTime.getHours() + ":" + (startTime.getMinutes() ? startTime.getMinutes() : "00") + "-" + endTime.getHours() + ":" + (endTime.getMinutes() ? endTime.getMinutes() : "00")
}

export default ShiftButton