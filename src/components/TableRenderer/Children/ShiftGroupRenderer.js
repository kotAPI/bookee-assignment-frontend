import React from "react";


import _ from "lodash";
import moment from "moment"

import ShiftButton from "./ShiftItemRenderer"

class ShiftGroupRenderer extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            groups: []
        }

        this.groupShifts = this.groupShifts.bind(this)
        
    }

    componentDidMount() {
       

    }

   

    groupShifts(shiftItems) {

        let validShifts = [];

        let now = new Date();
        shiftItems.forEach(shift => {

            let shiftDate = new Date(shift.endTime)
            if (shiftDate > now) {
                if( this.props.context==='AVAILABLE_SHIFTS'){
                    if(shift.area == this.props.activeTab && !shift.booked ){
                        validShifts.push(shift)
                    }
                   
                }else{
                    // booked shifts
                    if(shift.booked){
                        validShifts.push(shift)
                    }
                   
                }

            }
        });

        

        let groupedResults = _.groupBy(validShifts, (shift) => {
            let shiftDate = new Date(shift['startTime'])
            return moment(shiftDate, 'DD/MM/YYYY').startOf('day')
        });

        return Object.values(groupedResults)

    }


    render() {
        return (
            <div className="section" >
                {

                    this.groupShifts(this.props.shifts).map(group=>{
                        return (
                        <div className="item" key={group[0].id}>
                            <div className="title">
                                <span>{ moment(group[0].startTime).calendar(null,{
                                sameDay: '[Today]',
                                nextDay: '[Tomorrow]',
                                nextWeek: 'dddd'
                            }) }</span>
                            {this.props.context==="MY_SHIFTS"?<span className="title-info">{GetShiftInfo(group)}</span>:''}
                            </div>
                            <div className="items">
                                <span>{}</span>
                            {group.map(groupItem=>{
                                return(
                                    <div  key={groupItem.id}>
                                        <ShiftButton 
                                        shifts={this.props.shifts} 
                                        shiftChange={this.props.shiftChange} 
                                        shift={groupItem}
                                        context={this.props.context}/>
                                    </div>
                                    
                                )
                            })}
                            </div>
                            
                            
                        </div>
                    )
                    })
                }
            </div>
        )
    }
}


function GetShiftInfo(group){
    var string =group.length+" shifts, ";

    var totalTimeInMs = 0;

    _.forEach(group,groupItem=>{
        totalTimeInMs += new Date(groupItem.endTime)- new Date(groupItem.startTime)
    })

    var minutes = Math.floor((totalTimeInMs / (1000 * 60)) % 60)
    var hours = Math.floor((totalTimeInMs / (1000 * 60 * 60)) % 24);

  string += hours + " h " + minutes +" m";

  return string;
}


export default ShiftGroupRenderer