// @flow
import { useHistory } from "react-router-dom";
import "./_navbar.scss"


import { useLocation } from 'react-router-dom'


function Navbar() {
    const history = useHistory();

    function goHome(){
        history.push("/")
    }

    function goToAvailableShifts(){
        history.push("/available-shifts")
    }

    function GetLocationPath() {
        const location = useLocation();
      return location.pathname
      }

    return(
        <div className="navbar">
            <ul>
               
                <li onClick={goHome} className={"link "+(GetLocationPath()==='/'?'active':'')} >My Shifts</li>
                <li onClick={goToAvailableShifts} className={"link "+(GetLocationPath()==='/available-shifts'?'active':'')}>Available Shifts</li>
            </ul>
        </div>
    )
}

export default Navbar;